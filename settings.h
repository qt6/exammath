#ifndef SETTINGS_H
#define SETTINGS_H


#include <QList>

struct Settings{

    int minVal{0};
    int maxVal{0};
    int result{0};

    QList <char>operatorsList;

    int seconds{0};

};


#endif // SETTINGS_H
