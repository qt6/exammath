#ifndef EXAMPLE_H
#define EXAMPLE_H

struct Example{
    int fVal{0};
    int sVal{0};
    int rVal{0};
    int aVal{0};
    char oper{0};
    bool isRight{false};
};

#endif // EXAMPLE_H
