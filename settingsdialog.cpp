#include "settingsdialog.h"

#include <QApplication>
#include <QFormLayout>
#include <QPushButton>
#include <QTimeEdit>
#include <QSpinBox>
#include <QGroupBox>
#include <QCheckBox>
#include <QRadioButton>

#include "settings.h"

SettingsDialog::SettingsDialog(QWidget *parent)
    :QDialog(parent)
{
    auto mainLay=new QFormLayout(this);


    auto m_closeBtn=new QPushButton("Закрыть",this);
    m_aplyBtn=new QPushButton("Начать",this);

    m_minValSB=new QSpinBox(this);
    m_maxValSB=new QSpinBox(this);
    m_resultValSB=new QSpinBox(this);
    auto m_signBox=new QGroupBox(this);
    auto m_typeBox=new QGroupBox(this);

    auto typeLay=new QVBoxLayout(m_typeBox);

    m_onTimeRB=new QRadioButton("На время",m_typeBox);
    m_onCountRB=new QRadioButton("На количество",m_typeBox);
    m_countSB=new QSpinBox(m_typeBox);
    m_examTE=new QTimeEdit(this);

    auto signLay=new QGridLayout(m_signBox);

    m_plusCB=new QCheckBox("+",m_signBox);
    m_minusCB=new QCheckBox("-",m_signBox);
    m_multypleCB=new QCheckBox("*",m_signBox);
    m_devideCB=new QCheckBox("/",m_signBox);


    m_aplyBtn->setEnabled(false);

    m_countSB->setRange(0,1000);
    m_minValSB->setRange(0,1000);
    m_maxValSB->setRange(0,1000);
    m_resultValSB->setRange(0,1000);

    m_countSB->setButtonSymbols(QSpinBox::NoButtons);
    m_minValSB->setButtonSymbols(QSpinBox::NoButtons);
    m_maxValSB->setButtonSymbols(QSpinBox::NoButtons);
    m_resultValSB->setButtonSymbols(QSpinBox::NoButtons);
    m_examTE->setButtonSymbols(QSpinBox::NoButtons);

    m_countSB->setAlignment(Qt::AlignCenter);
    m_minValSB->setAlignment(Qt::AlignCenter);
    m_maxValSB->setAlignment(Qt::AlignCenter);
    m_resultValSB->setAlignment(Qt::AlignCenter);
    m_examTE->setAlignment(Qt::AlignCenter);

    m_examTE->setCurrentSection(QTimeEdit::MinuteSection);
    m_examTE->setDisplayFormat("hh:mm:ss");


    m_typeBox->setTitle("Вариант теста");

    typeLay->addWidget(m_onCountRB);
    typeLay->addWidget(m_countSB);
    typeLay->addWidget(m_onTimeRB);
    typeLay->addWidget(m_examTE);


    m_signBox->setTitle("Знаки");

    signLay->addWidget(m_plusCB,0,0);
    signLay->addWidget(m_minusCB,0,1);
    signLay->addWidget(m_multypleCB,1,0);
    signLay->addWidget(m_devideCB,1,1);


    mainLay->addRow("Минимальное значение",m_minValSB);
    mainLay->addRow("Максимальное значение",m_maxValSB);
    mainLay->addRow("Счет до",m_resultValSB);
    mainLay->addRow(m_typeBox);
    mainLay->addRow(m_signBox);
    mainLay->addRow(m_closeBtn,m_aplyBtn);



    m_signBox->setLayout(signLay);
    setLayout(mainLay);



    connect(m_aplyBtn,&QPushButton::clicked,[this](){
        Settings settings;

        settings.minVal=m_minValSB->value();
        settings.maxVal=m_maxValSB->value();
        settings.result=m_resultValSB->value();
        settings.seconds=m_examTE->time().hour()*3600
                +m_examTE->time().minute()*60
                +m_examTE->time().second();

        if(m_plusCB->isChecked())
            settings.operatorsList.append('+');
        if(m_minusCB->isChecked())
            settings.operatorsList.append('-');
        if(m_multypleCB->isChecked())
            settings.operatorsList.append('*');
        if(m_devideCB->isChecked())
            settings.operatorsList.append('/');


        settingsReceived(settings);
        close();

    });

    connect(m_plusCB,&QCheckBox::toggled,this,&SettingsDialog::checkSigns);
    connect(m_minusCB,&QCheckBox::toggled,this,&SettingsDialog::checkSigns);
    connect(m_multypleCB,&QCheckBox::toggled,this,&SettingsDialog::checkSigns);
    connect(m_devideCB,&QCheckBox::toggled,this,&SettingsDialog::checkSigns);
}

void SettingsDialog::checkSigns()
{
    m_aplyBtn->setEnabled(m_plusCB->isChecked()
                          ||m_minusCB->isChecked()
                          ||m_multypleCB->isChecked()
                          ||m_devideCB->isChecked()
                          );
}
