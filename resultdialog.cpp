#include "resultdialog.h"

#include <QApplication>
#include <QFormLayout>
#include <QTableWidget>
#include <QHeaderView>
#include <QPushButton>
#include <QSpinBox>
#include <QTimer>

#include "example.h"

ResultDialog::ResultDialog(QWidget *parent)
    :QDialog(parent)
{
    auto mainLay=new QFormLayout(this);
    timer=new QTimer(this);

    m_rightSB=new QSpinBox(this);
    m_wrongSB=new QSpinBox(this);
    m_countSB=new QSpinBox(this);

    secLeft=5;

    m_countRight=0;
    m_countWrong=0;

    resultLW=new QTableWidget(0,4,this);
    closeBtn=new QPushButton(QString("Закрыть (%0)").arg(secLeft),this);

    setMinimumSize(640,480);

    closeBtn->setEnabled(false);

    mainLay->addRow(resultLW);
    mainLay->addRow("Количество примеров",m_countSB);
    mainLay->addRow("Количество правильных ответов",m_rightSB);
    mainLay->addRow("Количество неправильных ответов",m_wrongSB);
    mainLay->addRow(closeBtn);

    setLayout(mainLay);


    m_countSB->setReadOnly(true);
    m_rightSB->setReadOnly(true);
    m_wrongSB->setReadOnly(true);

    m_countSB->setButtonSymbols(QSpinBox::NoButtons);
    m_rightSB->setButtonSymbols(QSpinBox::NoButtons);
    m_wrongSB->setButtonSymbols(QSpinBox::NoButtons);


    connect(closeBtn,&QPushButton::clicked,qApp,&QApplication::closeAllWindows);

    connect(timer,&QTimer::timeout,[this](){

        closeBtn->setText(QString("Закрыть (%0)").arg(secLeft));
        if(secLeft==0){
            closeBtn->setText("Закрыть");
            closeBtn->setEnabled(true);
            timer->stop();
        }
        secLeft--;
    });

    timer->start(1000);
}

ResultDialog::~ResultDialog()
{
    for(int row=0;row<resultLW->rowCount();row++){
        for(int col=0;col<resultLW->columnCount();col++){
            delete resultLW->item(row,col);
        }
    }
}

void ResultDialog::result(const QList<Example *> &results)
{
    resultLW->clear();
    resultLW->setHorizontalHeaderLabels(QStringList()<<"Пример"<<"Правильный ответ"<<"Твой ответ"<<"Проверка ответа");
    resultLW->setRowCount(results.size());

    resultLW->setEditTriggers(QTableWidget::NoEditTriggers);
    resultLW->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    for(int index=0;index<results.size();index++){
        const auto ex=results.at(index);
        resultLW->setItem(index,0,new QTableWidgetItem(QString("%0%1%2").arg(ex->fVal).arg(ex->oper).arg(ex->sVal)));
        resultLW->setItem(index,1,new QTableWidgetItem(QString("%0").arg(ex->rVal)));
        resultLW->setItem(index,2,new QTableWidgetItem(QString("%0").arg(ex->aVal)));
        auto tmpItem=new QTableWidgetItem;
        if(ex->isRight){
            tmpItem->setText("Верно");
            tmpItem->setBackground(QColor(127,255,0));
            m_countRight++;
        }else{
            tmpItem->setText("Не верно");
            tmpItem->setBackground(QColor(252,40,71));
            m_countWrong++;
        }

        resultLW->setItem(index,3,tmpItem);
    }


    m_countSB->setValue(results.size());
    m_rightSB->setValue(m_countRight);
    m_wrongSB->setValue(m_countWrong);


}

//void ResultDialog::closeEvent(QCloseEvent *e)
//{
//    QApplication::closeAllWindows();
////    QDialog::closeEvent(e);
//}
