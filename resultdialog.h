#ifndef RESULTDIALOG_H
#define RESULTDIALOG_H

#include <QDialog>
#include <QList>

class Example;
class QTableWidget;
class QPushButton;
class QSpinBox;
class QTimer;

class ResultDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ResultDialog(QWidget *parent =nullptr);
    ~ResultDialog();

    void result(const QList<Example *> &results);

private:

//    virtual void closeEvent(QCloseEvent *e)override;

    QTableWidget *resultLW;
    QPushButton *closeBtn;
    int secLeft;
    QTimer *timer;

    QSpinBox *m_countSB;
    QSpinBox *m_rightSB;
    QSpinBox *m_wrongSB;

    int m_countRight;
    int m_countWrong;


};

#endif // RESULTDIALOG_H
