QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++2a

DESTDIR = $${OUT_PWD}/../$${TARGET}


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    examplewidget.cpp \
    resultdialog.cpp \
    settingsdialog.cpp

HEADERS += \
    example.h \
    examplewidget.h \
    resultdialog.h \
    settings.h \
    settingsdialog.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


Release:win32:QMAKE_POST_LINK += windeployqt $$OUT_PWD/$$DESTDIR
